/* eslint-disable prettier/prettier */
import React from 'react';
const BlogContext = React.createContext();
export const BlogProvider = ({ children }) => {
    return <BlogContext.Provider value={12}>
        {children}
    </BlogContext.Provider>;
};
export default BlogContext;
